Shader "Unlit/WeaModelShader"
{
    Properties
    {
        _LightColor("LightColor", Color) = (1, 1, 1)
        _ThresholdSky("ThresholdSky", Range(0.0, 1.0)) = 1.0
        _BlueValSky("BlueValSky", float) = 1.0
        _ThresholdCloud("ThresholdCloud", Range(0.0, 1.0)) = 1.0
        _BlueValCloud("BlueValCloud", float) = 1.0
        _BaseMap("Base Map", 2D) = "white"
        _SampleMap("Sample Map", 2D) = "black"
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline" }

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionHCS  : SV_POSITION;
                float2 uv           : TEXCOORD0;
                float2 normalizedScreenSpace : TEXCOORD1;
                half4 sampleColorArray[4] : COLOR1;
            };

            TEXTURE2D(_BaseMap);
            SAMPLER(sampler_BaseMap);

            TEXTURE2D(_SampleMap);
            SAMPLER(sampler_SampleMap);

            CBUFFER_START(UnityPerMaterial)
                float _ThresholdSky;
                float _BlueValSky;
                float _ThresholdCloud;
                float _BlueValCloud;
                float4 _BaseMap_ST;
                float4 _SampleMap_ST;
                float4 _SampleMap_TexelSize;
            CBUFFER_END

            Varyings vert(Attributes IN)
            {
                Varyings OUT;
                OUT.positionHCS = TransformObjectToHClip(IN.positionOS.xyz);
                OUT.uv = IN.uv;
                return OUT;
            }

            half4 frag(Varyings IN) : SV_Target
            {
                float2 normalizedScreenSpace = GetNormalizedScreenSpaceUV(IN.positionHCS);
                float dx = ddx(normalizedScreenSpace.xy * _SampleMap_TexelSize.zw);
                float dy = ddy(normalizedScreenSpace.xy * _SampleMap_TexelSize.zw);
                IN.sampleColorArray[0] = SAMPLE_TEXTURE2D(_SampleMap, sampler_SampleMap, normalizedScreenSpace + dy + dy);
                IN.sampleColorArray[1] = SAMPLE_TEXTURE2D(_SampleMap, sampler_SampleMap, normalizedScreenSpace - dy + dy);
                IN.sampleColorArray[2] = SAMPLE_TEXTURE2D(_SampleMap, sampler_SampleMap, normalizedScreenSpace - dy - dy);
                IN.sampleColorArray[3] = SAMPLE_TEXTURE2D(_SampleMap, sampler_SampleMap, normalizedScreenSpace + dy - dy);
                float distanceSky = 0.0f;
                float distanceCloud = 0.0f;
                [unroll] for (int i = 0; i < 4; i++) {
                    distanceSky = abs(IN.sampleColorArray[i].b - _BlueValSky);
                    distanceCloud = abs(IN.sampleColorArray[i].b - _BlueValCloud);
                    if (distanceSky > _ThresholdSky && distanceCloud > _ThresholdCloud) discard;
                }
                half4 color = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, IN.uv);
                return color;
            }
            ENDHLSL
        }
    }
}